#!/bin/bash

# Set the threshold for the number of ICMP echo requests
THRESHOLD=10
BAN_TIME=600 # 10 minutes ban time

# Run tcpdump to capture ICMP echo requests
tcpdump -l -n icmp[icmptype]=icmp-echo &

# Read tcpdump output line by line
while IFS= read -r line; do
    # Check if the line contains ICMP echo request
    if echo "$line" | grep -q 'ICMP echo request'; then
        # Extract the source IP address
        ip=$(echo "$line" | awk '{print $3}')

        # Increment the count for this IP address
        ((count["$ip"]++))

        # Check if the count exceeds the threshold
        if (( count["$ip"] >= THRESHOLD )); then
            echo "IP address $ip exceeded threshold. Banning..."
            # Add IP address to firewall blacklist (example command)
            iptables -A INPUT -s "$ip" -j DROP
            # Sleep for ban time
            sleep "$BAN_TIME"
            # Remove IP address from blacklist (example command)
            iptables -D INPUT -s "$ip" -j DROP
            # Reset the count
            count["$ip"]=0
        fi
    fi
done
