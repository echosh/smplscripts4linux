#!/bin/bash

cidr="$1"
if [[ ! "$cidr" =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/[0-9]+$ ]]; then
    echo "Invalid CIDR notation. Example: 10.200.20.0/27"
    exit 1
fi

# Extract the IP address and subnet mask length from CIDR notation
ip="${cidr%/*}"
mask_length="${cidr##*/}"

# Convert the IP address to binary
IFS='.' read -r -a ip_octets <<< "$ip"
ip_binary=""
for octet in "${ip_octets[@]}"; do
    octet_binary="$(printf "%08d" $(bc <<< "obase=2;$octet"))"
    ip_binary+="$octet_binary"
done

# Create the subnet mask
subnet_mask=""
for (( i=0; i<32; i++ )); do
    if (( i < mask_length )); then
        subnet_mask+="1"
    else
        subnet_mask+="0"
    fi
done

# Invert the subnet mask
inverted_subnet_mask=""
for (( i=0; i<32; i++ )); do
    if [[ "${subnet_mask:$i:1}" == "0" ]]; then
        inverted_subnet_mask+="1"
    else
        inverted_subnet_mask+="0"
    fi
done

# Calculate the broadcast address
broadcast_address=""
for (( i=0; i<32; i+=8 )); do
    octet="${inverted_subnet_mask:$i:8}"
    broadcast_address+=".$((2#"$octet"))"
done

broadcast_address="${broadcast_address:1}"  # remove leading dot
echo "$broadcast_address"

