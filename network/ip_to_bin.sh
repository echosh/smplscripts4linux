#!/bin/bash

ip="$1"
min_digits="$2"
if [ -z "$min_digits" ]; then
    min_digits=8  # set a default
fi

binary_str=""
IFS='.' read -r -a octets <<< "$ip"
for octet in "${octets[@]}"; do
    binary_octet="$(printf "%08d" $(echo "obase=2; $octet" | bc))"
    binary_str="${binary_str}${binary_octet} "
done
binary_str="${binary_str%?}"  # remove the trailing dot

echo "$binary_str"

# Nubmer to binary
#
# num_in="$1"
# min_digits="$2"
# if [ -z "$min_digits" ]; then
#     min_digits=8  # set a default
# fi
#
# binary_str="$(echo "obase=2; $num_in" | bc)"
#
# num_chars="${#binary_str}"
# num_zeros_to_add_as_prefix=$((min_digits - num_chars))
# zeros=""
# for (( i=0; i<"$num_zeros_to_add_as_prefix"; i++ )); do
#     zeros="${zeros}0"  # append another zero
# done
#
# binary_str="${zeros}${binary_str}"
# echo "$binary_str"

# toBin() {
#     printf "%08d\n" $(dc -e "$1 2op")
# }
