#!/bin/bash

cidr="$1"
if [[ ! "$cidr" =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/[0-9]+$ ]]; then
    echo "Invalid CIDR notation. Example: 10.200.20.0/27"
    return 1
fi

# Extract the subnet mask length from CIDR notation
mask_length="${cidr##*/}"

# Convert the subnet mask length to a subnet mask
subnet_mask=""
for (( i=0; i<32; i++ )); do
    if (( i < mask_length )); then
        subnet_mask+="1"
    else
        subnet_mask+="0"
    fi
done

echo "$subnet_mask"

# Convert the binary subnet mask to decimal
decimal_subnet_mask=""
for (( i=0; i<4; i++ )); do
    octet="${subnet_mask:$((i*8)):8}"
    decimal_subnet_mask+="$(echo "ibase=2; $octet" | bc)."
done
decimal_subnet_mask="${decimal_subnet_mask%?}"  # remove the trailing dot

echo "$decimal_subnet_mask"
