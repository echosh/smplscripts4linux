#!/bin/bash
#
myip_opendns="$(dig +short myip.opendns.com @resolver1.opendns.com)"
#myip_host="$(host myip.opendns.com resolver1.opendns.com)"
myip_cloudflare="$(dig +short txt ch whoami.cloudflare @1.0.0.1)"
#myip_ipv6="$(dig -6 TXT +short o-o.myaddr.l.google.com @ns1.google.com)"
#myip_ipv6="$(ip -6 addr | grep inet6 | awk -F '[ \t]+|/' '{print $3}' | grep -v ^::1 | grep -v ^fe80)"
myip_ipv6="$(ip -6 addr show | grep inet6)"
echo "My WAN/Public IP address(opendns): ${myip_opendns}"
echo "My WAN/Public IP address(cloudflare): ${myip_cloudflare}"
echo -e "IPv6 address: \n${myip_ipv6}"
