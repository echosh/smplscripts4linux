#!/bin/bash

while [ $# -gt 0 ]
do
  case "$1" in
    -h|--help )
      echo "Usage: $0 [-h] [--help] [-f file] [--file file]"
      shift #throw away -h or --help
      exit 1
      ;;

    -f|--file )
      FILE=$2
      if ! [ -f "$FILE" ]; then
        echo "File does not exists"
        exit 2
      fi

      LINES=`cat "$FILE" | wc -l`
      WORDS=`cat "$FILE" | wc -w`
      LETTERS=`cat "$FILE" | wc -m`

      echo "=====FILE: $FILE====="
      echo "LINES: $LINES"
      echo "WORDS: $WORDS"
      echo "LETTERS: $LETTERS"
      shift #throw away parameter argument
      shift #throw away parameter value
      ;;

    * )
      echo "Use -h or --help for more info"
      exit 3
      ;;
  esac
done


