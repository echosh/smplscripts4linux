#!/bin/bash

ARRAY=($(ls *.sh))
COUNT=0

echo -e "FILE NAME \t WRITEABLE"

for FILE in "${ARRAY[@]}"
do
  echo -n $FILE
  echo -ne "[${#ARRAY[$COUNT]}]\t"
  if [ -w "$FILE" ]; then
    echo "YES"
  else
    echo "NO"
  fi

  let COUNT++
done
