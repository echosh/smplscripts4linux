#!/bin/bash

NUMBER=23

let RESULT_1=NUMBER+32

RESULT_2=$(( NUMBER + 32 ))

RESULT_3=$[ NUMBER + 32 ]
RESULT_4=`expr $NUMBER + 32`
RESULT_5=$(echo "$NUMBER * 1.23" | bc)

echo $RESULT_{1..5}

echo -n "Enter number for sum to result: "
read IN_NUM
let RESULT_PLUS=RESULT_1+IN_NUM
echo "$RESULT_1 + $IN_NUM = $RESULT_PLUS"

EXPON=`echo "$NUMBER^$RESULT_PLUS" | bc`
echo -n $EXPON
