#!/bin/bash

if [ $# -lt 1 ]; then
  echo "Please, add parameters or enter '$0 -h/--help'"
  exit 1
fi

function usage() {
  echo "Usage: $0 [-l location] [--location location] [-e extension] [--extension extension] [-h help] [--help help]"
  echo "Examples:"
  echo "$0 -l /etc/ -e txt -s"
  echo "$0 -e img --stats"
  echo 
  exit 2
}

LOC_SET=0    #0-location not set, use current location; 1-location set
STATS=0      #0/1-display or not

while [ $# -gt 0 ]
do
  case $1 in

    -l|--location )
      LOCATION="$2"
      if ! [ -d "$LOCATION" ]; then
        usage
      fi
      LOC_SET=1
      shift
      shift
      ;;

    -e|--extension )
      EXT="$2"
      shift
      shift
      ;;

    -s|--stats )
      STATS=1
      shift
      ;;

    -h|--help )
      shift
      usage
      ;;

    * )
      usage
      ;;

  esac
done


if [ "$LOC_SET" -ne 1 ]; then
  LOCATION=$(pwd)
fi

echo "LOCATION: $LOCATION"

if [ "$EXT" != "" ]; then
  ls -l $LOCATION | awk '/^-/' | grep "\.$EXT$" &>/dev/null
  if [ $? -ne 0 ]; then
    echo "No file with extension: $EXT found"
    exit 3
  fi

  ls -l $LOCATION | awk '/^-/' | grep "\.$EXT$" | awk -v stats=$STATS -f "$(pwd)/size.awk"
else
  ls -l $LOCATION | awk '/^-/' | awk -v stats=$STATS -f "$(pwd)/size.awk"
fi
