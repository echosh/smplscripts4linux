#!/bin/bash

# Default mount directory
DEFAULT_MOUNT_DIR="/mnt"

# Function to display available block devices and partitions
list_devices() {
    # List block devices, including partitions, filtering by device names and ignoring loop devices
    lsblk -no NAME,TYPE | grep -E 'part|disk' | grep -v "loop" | sed 's/[│├└─]//g' | awk '{print $1}'
}

# Display block devices as a menu using select
select_device() {
    PS3="Please choose a device or partition to mount (or press Ctrl+C to quit): "
    echo "Available devices and partitions to mount:"

    # Use select to generate a menu from available devices and partitions
    select device in $(list_devices); do
        if [[ -n "$device" ]]; then
            echo "You have selected: /dev/$device"
            return 0
        else
            echo "Invalid selection. Please try again."
        fi
    done
}

# Mount the selected device
mount_device() {
    local device=$1
    local mount_point

    # Default mount directory based on device name (e.g., /mnt/sda1)
    default_mount_point="$DEFAULT_MOUNT_DIR/$device"

    # Ask for the mount point with tab completion enabled
    echo "Tip: You can use tab for autocompletion when entering the path."
    read -e -p "Enter the path where you want to mount the device (press Enter for default: $default_mount_point): " mount_point

    # If no mount point provided, use the default based on device name
    if [[ -z "$mount_point" ]]; then
        mount_point=$default_mount_point
    fi

    # Create the mount directory if it doesn't exist
    if [[ ! -d "$mount_point" ]]; then
        echo "Creating mount directory: $mount_point"
        mkdir -p "$mount_point"
    fi

    # Mount the device
    sudo mount /dev/$device "$mount_point"

    # Check if the mount was successful
    if [[ $? -eq 0 ]]; then
        echo "Device /dev/$device successfully mounted to $mount_point"
    else
        echo "Failed to mount /dev/$device."
        exit 1
    fi
}

# Main logic of the script
echo "Welcome to the device mounting script!"

echo "lsblk output:"
lsblk

# Allow selecting and mounting multiple devices
for i in 1 2 3; do
    echo "Mounting device $i of 3"

    # Call function to select a device
    select_device

    # Call function to mount the selected device
    mount_device "$device"
done
