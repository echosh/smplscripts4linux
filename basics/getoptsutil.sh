#!/bin/bash

echo "All arguments: $@"
opts=`getopt -o a::b:cde --long file::,name:,help -- "$@"`
eval set -- "$opts"
echo "All arguments after getopt: $@"

while getopts a::b:cd param; do
  case $param in
    a) echo "getopts parameter 'a' with argument: $OPTARG"
      ;;
    b) echo "getopts parameter 'b' with argument: $OPTARG"
      ;;
    c) echo "getopts parameter 'c' selected (no colon, no arg)"
      ;;
    d) echo "getopts parameter 'd' selected"
      ;;
  esac
done

while [ $# -gt 0 ]
do
  case "$1" in
    -a) echo "getopt parameter 'a' with argument: $2"
      shift 2
      ;;
    -b) echo "getopt parameter 'b' with argument: $2"
      shift 2
      ;;
    -c) echo "getopt parameter 'c' selected (no colon, no arg)"
      shift
      ;;
    -d) echo "getopt parameter 'd' selected"
      shift
      ;;
    -e) echo "getopt parameter 'e' selected"
      shift
      ;;
    --file) echo "getopt parameter 'file' with argument: $2"
      shift 2
      ;;
    --name) echo "getopt parameter 'name' with argument: $2"
      shift 2
      ;;
    --help) echo "getopt parameter 'help' selected (no colon, no arg)"
      shift
      ;;
    *) echo "something alse provided: $1"
      shift
      ;;
  esac
done

