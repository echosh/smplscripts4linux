#!/bin/bash

# args check
if [ $# -ne 1 ]; then
  echo "One argument needed for $0"
  exit 1
fi

FILE=$1

if [ -f $FILE ]; then

  #default vars
  READABLE="NO"
  WRITABLE="NO"
  EXECUTABLE="NO"

  
  if [ -r $FILE ]; then
    READABLE="YES"
  fi
  
  if [ -w $FILE ]; then
    WRITABLE="YES"
  fi
  
  if [ -x $FILE ]; then
    EXECUTABLE="YES"
  fi

  echo ===FILE: $FILE===
  echo "READ:     $READABLE"
  echo "WRITE:    $WRITABLE"
  echo "EXECUTE:  $EXECUTABLE"

else
  if [ -d $FILE ]; then
    echo $FILE is a directory
  else
    echo $FILE does not exists
  fi
fi
