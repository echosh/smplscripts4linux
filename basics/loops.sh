#!/bin/bash

if [ ! $# -eq 1 ]; then
  echo "Two arguments needed!"
  exit 1
fi

if ! [ -f "$1" ]; then
  echo "File does not exists"
  exit 2
fi

FILENAME=$1
COUNT_LINES=1
COUNT=1

while read line
do
  echo "$COUNT_LINES: $line"
  let COUNT_LINES++
done < "$FILENAME"

for ARG in "$@"
do
  echo "$COUNT. argument: $ARG"
  let COUNT++
done

#for FILE in *.txt
#do
  #echo $(date) >> $FILE
  #ps -ef | head -5 >> $FILE
  #echo ================== >> $FILE
#done

while [ "$NAME" != "q" ]
do

  echo -n "Enter your name or quit(q): "
  read NAME
  #if [ "$NAME" == "q" ]; then
    #echo Bye!
    #exit 3
  #fi

  echo -n "Correct?(y/n): "
  read NAME
  case "$NAME" in

    "Y" | "y" )
      echo "NAME: $NAME"
      exit 3
      ;;

    N|n )
      continue
      ;;

    * )
      echo "Wrong answer provided"
      ;;

  esac

done
