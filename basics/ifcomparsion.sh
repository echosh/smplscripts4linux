#!/bin/bash

if [ ! $# -eq 2 ]; then
  echo "Two arguments needed!"
  exit 1
fi

FIRST=$1
SECOND=$2

let RESULT=FIRST*SECOND
if [ $? -ne 0 ]; then
  echo "You need to put 2 integers!"
  exit 2
else
  echo "$FIRST * $SECOND = $RESULT"
fi

# echo "Exit status from \"let\": $?"

read -p "STR_1: " STR1
read -p "STR_2: " STR2

[[ $STR1 == $STR2 ]] && echo "Equal" || echo "Not equal"

WHOAMI=$(whoami)

if [ "$WHOAMI" != "root" ]; then
  echo "You are not root, exiting"
  exit 3
fi

ps -ef | head -4
