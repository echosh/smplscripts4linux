#!/bin/bash

# args check
if [ $# -ne 1 ]; then
  echo "One argument needed for $0"
  exit 1
fi

VAR=$1

# ip address regex
REGEX="[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[[:digit:]]{1,3}"

# regex check
if ! [[ $VAR =~ $REGEX ]]; then
  echo "No IP address provided"
  exit 2
fi

IP=${BASH_REMATCH[0]}

# check if ip address is reachable or not
ping -c3 $IP 1>/dev/null
if [ $? -eq 0 ]; then
  STATUS="ALIVE"
else
  STATUS="DEAD"
fi

echo $IP $STATUS
