#!/bin/bash

# ./spaces.sh [file-path] [-f|--fix] [-h|--help]

function usage() {
  echo "Usage: $0 [file-path] [-f|--fix] [-h|--help]"
  exit 1
}

if [ $# -eq 0 ]; then
  usage
fi

FIX=0

while [ $# -gt 0 ]
do
  case "$1" in

    -f|--fix )
      FIX=1
      shift
      ;;

    -h|--help )
      usage
      shift
      ;;

    * )
      if [ -f "$1" ]; then
        FILE="$1"
        shift
      else
        usage
      fi
      ;;

    esac
done

if [ $FIX -eq 1 ] && [ -f "$FILE" ]; then
  echo "Fixing spaces and tabs at the beginning and at the and of lines"
  sed -i 's/[[:blank:]]\+$//' "$FILE"
  sed -i 's/^[[:blank:]]\+//' "$FILE"
fi

if [ -f "$FILE" ]; then 
  LINES_NUM=0
  REGEX_START="^[[:blank:]]+"
  REGEX_END="[[:blank:]]+$"

  while IFS= read -r line
  do
    let LINES_NUM++

    echo "$line" | sed -e '/[[:blank:]]\+$/q9' -e '/^[[:blank:]]\+/q7' >/dev/null

    if [ $? -eq 0 ]; then
      printf %4s "$LINES_NUM: " >> temp.txt
      echo "$line" >> temp.txt
      continue
    fi

    printf %4s "$LINES_NUM: " >> temp.txt

    if [[ "$line" =~ $REGEX_START ]]; then
      MATCH=`echo "$BASH_REMATCH" | sed 's/\t/|_TAB_|/g'`
      echo -e -n "\e[41m$MATCH\e[49m" >> temp.txt
    fi

    echo -e -n "$line" | sed -e 's/^[[:blank:]]\+//' -e 's/[[:blank:]]\+$//' >> temp.txt

    if [[ "$line" =~ $REGEX_END ]]; then
      MATCH=`echo "$BASH_REMATCH" | sed 's/\t/|_TAB_|/g'`
      echo -e "\e[41m$MATCH\e[49m" >> temp.txt
    else
      echo >> temp.txt
    fi


  done < "$FILE"

  cat temp.txt
  rm temp.txt

fi

if [ $FIX -eq 1 ]; then
  echo
  echo -e "\e[105mDONE!\e[49m"
fi
