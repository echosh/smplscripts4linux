#!/bin/bash

IFS=','
echo "script name: $0"
echo "first arg: $1"
echo "second arg: $2"
echo "all args with \$@: $@"
echo "all args with \$*: $*"
echo "count args with \$#: $#"

# asign args to variables
FIRST=$1
SECOND=$2

ARGS_SUM=$[ FIRST + SECOND ]
echo $ARGS_SUM
