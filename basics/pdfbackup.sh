#!/bin/bash

PROD=/home/idapp/Templates

# args check
if [ $# -ne 1 ]; then
  echo "One argument needed $0\/destination_path"
  exit 1
fi

# check destination_path
DESTINATION=$1
if [[ $DESTINATION != */backup ]]; then
  echo Wrong destination path, must ends with /backup
  exit 2
fi

# create destination folder
DATE=$(date +%Y_%m_%d_%H-%M)
mkdir -p $DESTINATION/$DATE

# copy 
cp $PROD/*.pdf $DESTINATION/$DATE

# Test
if [ $? -eq 0 ]; then
  echo backup OK
else
  echo backup failed
fi
