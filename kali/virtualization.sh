#!/bin/bash

# Update the package list
sudo apt update

# List of tools to install
tools=(
    qemu-kvm
    qemu-system
    qemu-user-static
    qemu-guest-agent
    virt-manager
    virt-viewer
    dnsmasq
    vde2
    bridge-utils
    netcat-openbsd
    libguestfs-tools
    libvirt-daemon-system
    libvirt-clients

)

# Install each tool if it's not already installed
for tool in "${tools[@]}"; do
    if ! dpkg -l | grep -q "^ii  $tool "; then
        echo "Installing $tool..."
        sudo apt install -y "$tool"
    else
        echo "$tool is already installed."
    fi
done

echo "All tools installation checks complete."

sudo adduser $USER libvirt
sudo adduser $USER kvm

echo "User $USER added to libvirt and kvm groups."
