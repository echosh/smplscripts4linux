#!/bin/bash

# Check if a directory path is provided as a command-line argument
if [ $# -eq 0 ]; then
  echo "Usage: $0 /path/to/your/directory"
  exit 1
fi

# Specify the directory containing the files
directory="$1"

# Navigate to the directory
cd "$directory" || exit

# for file in *; do
#   # Check if the file name contains ' English'
#   if [[ "$file" == *" English"* ]]; then
#     # Remove ' English' from the file name and rename the file
#     new_name="${file// English}"
#     mv "$file" "$new_name"
#     echo "Renamed: $file -> $new_name"
#   else
#     echo "No change needed for: $file"
#   fi
# done

# Iterate over all files in the current directory
for file in *; do
  # Check if the file name matches the pattern where numbers follow any prefix
  if [[ $file =~ [0-9]+ ]]; then
    # Extract the numeric part of the filename
    new_name=$(echo "$file" | sed 's/^[^0-9]*//')
    
    # Rename the file if the new name is different from the old name
    if [[ "$file" != "$new_name" ]]; then
      mv "$file" "$new_name"
      echo "Renamed '$file' to '$new_name'"
    fi
  fi
done
