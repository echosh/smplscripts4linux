#!/bin/bash

# Install RPM Fusion repositories for Fedora
echo "Installing RPM Fusion Free and Non-Free Repositories..."
sudo dnf install -y \
    https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm \
    https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

# Enable fedora-cisco-openh264 repository
echo "Enabling fedora-cisco-openh264 repository..."
sudo dnf config-manager --enable fedora-cisco-openh264

# Update the @core group packages
echo "Updating core group packages..."
sudo dnf update -y @core

echo "All tasks completed successfully."
