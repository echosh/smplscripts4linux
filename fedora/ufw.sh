#!/bin/bash

# Install UFW if not installed (dnf for Fedora)
if ! command -v ufw &> /dev/null; then
  echo "UFW is not installed. Installing UFW using dnf..."
  sudo dnf install -y ufw
fi

# Disable UFW if it's enabled
echo "Disabling UFW if it's currently enabled..."
sudo ufw disable

# Reset all firewall rules
echo "Resetting UFW firewall rules..."
sudo ufw --force reset

# Set firewall rules
echo "Setting new firewall rules..."
sudo ufw limit 22/tcp             # Limit SSH connections
sudo ufw allow 80/tcp             # Allow HTTP traffic
sudo ufw allow 443/tcp            # Allow HTTPS traffic
sudo ufw deny 631/tcp             # Block CUPS port 631 (both TCP and UDP)
sudo ufw deny 631/udp

# Default policies
sudo ufw default deny incoming    # Deny all incoming traffic by default
sudo ufw default allow outgoing   # Allow all outgoing traffic by default

# Enable UFW at system startup
echo "Enabling UFW to start on system boot..."
sudo systemctl enable ufw.service

# Start UFW immediately
echo "Starting UFW service..."
sudo systemctl start ufw.service
echo "Firewall is enabled."

# Disable and remove CUPS services
echo "Disabling CUPS service..."
sudo systemctl stop cups
sudo systemctl disable cups
echo "Removing CUPS service..."
sudo dnf remove -y cups

# List all firewall rules
sudo ufw status verbose

echo "Setup complete. Firewall is active and CUPS is blocked."

