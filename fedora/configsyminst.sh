#!/bin/bash

# Define your config directory root (this should be the path to your repo)
CONFIG_DIR=$(pwd)

# List of tools and corresponding config directories
declare -A tools=(
  [alacritty]="alacritty"
  [bat]="bat"
  [dunst]="dunst"
  [fish]="fish"
  [i3]="i3"
  [nvim]="nvim"
  [picom]="picom"
  [polybar]="polybar"
  [ranger]="ranger"
  [rofi]="rofi"
  [tmux]="tmux"
  [translate-shell]="translate-shell"
  [ufetch]="ufetch-fedora"
  [vim]="vim"
  [zathura]="zathura"
)

# Function to install tool if not installed
install_tool() {
  tool=$1
  echo "Checking if $tool is installed..."
  if ! command -v $tool &> /dev/null; then
    echo "$tool is not installed. Installing..."
    if [[ -x "$(command -v dnf)" ]]; then
      sudo dnf install -y $tool
    elif [[ -x "$(command -v apt-get)" ]]; then
      sudo apt-get install -y $tool
    elif [[ -x "$(command -v yum)" ]]; then
      sudo yum install -y $tool
    else
      echo "Error: Package manager not found. Cannot install $tool."
      return 1
    fi
  else
    echo "$tool is already installed."
  fi
}

# Function to symlink config
symlink_config() {
  tool=$1
  config_path="$CONFIG_DIR/${tools[$tool]}"
  target_dir="$HOME/.config/$tool"
  
  if [ -d "$target_dir" ]; then
    echo "$target_dir already exists. Backing up as ${target_dir}.bak"
    mv "$target_dir" "${target_dir}.bak"
  fi
  
  echo "Creating symlink for $tool config: $config_path -> $target_dir"
  ln -s "$config_path" "$target_dir"
}

# Iterate over each tool and ask user if they need the config
for tool in "${!tools[@]}"; do
  read -p "Do you need the configuration for $tool? (Y/n) " choice
  case "$choice" in
    y|Y|"" )
      install_tool $tool && symlink_config $tool
      ;;
    n|N )
      echo "Skipping $tool."
      ;;
    * )
      echo "Invalid choice. Skipping $tool."
      ;;
  esac
done

