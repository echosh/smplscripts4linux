#!/bin/bash

# Update system package index
echo "Updating system packages..."
sudo dnf update -y

# Upgrade all installed packages
echo "Upgrading installed packages..."
sudo dnf upgrade --refresh -y

# Remove any unused dependencies and orphan packages
echo "Removing unused and orphan dependencies..."
sudo dnf autoremove -y

# Clean up package cache to free up disk space
echo "Cleaning up package cache..."
sudo dnf clean all

echo "System update, upgrade, and cleanup completed!"
