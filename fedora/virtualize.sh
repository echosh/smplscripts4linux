#!/bin/bash

#!/bin/bash

# Step 1: Install Virtualization Package Group
echo "Installing Virtualization group packages..."
sudo dnf install -y @virtualization

# Step 2: Edit the libvirtd configuration
echo "Configuring libvirtd..."

# Backing up the original configuration file
sudo cp /etc/libvirt/libvirtd.conf /etc/libvirt/libvirtd.conf.bak

# Modify the libvirtd.conf file
sudo sed -i 's/^#unix_sock_group =.*/unix_sock_group = "libvirt"/' /etc/libvirt/libvirtd.conf
sudo sed -i 's/^#unix_sock_rw_perms =.*/unix_sock_rw_perms = "0770"/' /etc/libvirt/libvirtd.conf

echo "libvirtd configuration updated."

# Step 3: Start and enable the libvirtd service
echo "Starting and enabling the libvirtd service..."
sudo systemctl start libvirtd
sudo systemctl enable libvirtd

# Step 4: Add current user to the libvirt group
echo "Adding current user to the libvirt group..."
sudo usermod -aG libvirt $(whoami)

echo "You must log out and log in again to apply the group changes."

# Confirm completion
echo "Setup completed successfully!"
