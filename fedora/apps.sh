#!/bin/bash

# Check if 'dnf' is available
if ! command -v dnf &>/dev/null; then
	echo "Error: 'dnf' is not installed or not available in PATH."
	exit 1
fi

echo "Using 'dnf' for package management."

echo "Removing i3-lock"
sudo dnf remove -y i3lock

# Define a function to check if a package is installed
check_installed() {
	local package=$1
	if ! rpm -q $package &>/dev/null; then
		echo "Package '$package' is not installed. Installing..."
		sudo dnf install -y $package || {
			echo "Failed to install $package. Exiting."
			exit 1
		}
	fi
}

# List of packages to install
packages=(
  nasm
  iftop
	tmux
	bc
	tldr
	fzf
	feh
	xset
	xautolock
	dunst
	alacritty
	kitty
	rofi
	maim
	unclutter
	ranger
	jq
	ntfs-3g
	go
	fd-find
	zathura
	zathura-cb
	zathura-djvu
	zathura-pdf-mupdf
	zathura-ps
	zoxide
	task
	vit
	python3-pip
)

# Check and install each package
for package in "${packages[@]}"; do
	check_installed $package
done

pip3 install neovim i3-layouts

echo "All packages installed."
