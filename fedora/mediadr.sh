#!/bin/bash

# Swap ffmpeg-free with full ffmpeg package, allowing erasing of conflicting packages
echo "Swapping ffmpeg-free with full ffmpeg..."
sudo dnf swap -y ffmpeg-free ffmpeg --allowerasing

# Update multimedia group packages without installing weak dependencies and excluding PackageKit-gstreamer-plugin
echo "Updating multimedia group packages..."
sudo dnf update -y @multimedia --setopt="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin

# Update sound and video group packages
echo "Updating sound and video group packages..."
sudo dnf update -y @sound-and-video

# Install Intel Media Driver (for Intel GPUs)
echo "Installing Intel Media Driver..."
sudo dnf install -y intel-media-driver

# Swap Mesa VA drivers to freeworld versions for better video acceleration support
echo "Swapping Mesa VA drivers to freeworld versions..."
sudo dnf swap -y mesa-va-drivers mesa-va-drivers-freeworld

# Swap Mesa VDPAU drivers to freeworld versions for better video acceleration support
echo "Swapping Mesa VDPAU drivers to freeworld versions..."
sudo dnf swap -y mesa-vdpau-drivers mesa-vdpau-drivers-freeworld

echo "All tasks completed successfully."

