#!/bin/bash

if [ $# -lt 2 ]; then
  echo "Add mountpoint and bind pathes, or enter '$0 -h/--help"
  exit 1
fi

function usage() {
  echo "Usage: $0 [-m/--mountpoint -> location of mountpoint] [-b/--bind -> directory to bind]"
  echo "Examples:"
  echo "$0 -m /mnt/hdd/ -b /home/user/hdddir"
  echo
  exit 2
}

MOUNT_PATH=0
BIND_PATH=0

while [ $# -gt 0 ] 
do
  case $1 in
    -m|--mountpoint )
      MOUNTPOINT="$2"
      if ! [ -d "$MOUNT_PATH" ]; then
        usage
      fi
      MOUNT_PATH=1
      shift
      shift
      ;;

    -b|--bind )
      BINDPOINT="$2"
      if ! [ -d "$BIND_PATH" ]; then
        usage
      fi
      BIND_PATH=1
      shift
      shift
      ;;

    -h|--help )
      shift
      usage
      ;;

    * )
      usage
      ;;

  esac
done

if [ -d "/mount" ]; then
    USERNAME=$(whoami)
    SUBDIRECTORY="/mount/${USERNAME}_hdd"

    # Check if the subdirectory already exists
    if [ -d "$SUBDIRECTORY" ]; then
        echo "Subdirectory '$SUBDIRECTORY' already exists."
    else
        # Create the subdirectory
        mkdir "$SUBDIRECTORY"
        echo "Subdirectory '$SUBDIRECTORY' created."
    fi
else
    echo "Error: /mount directory does not exist."
fi


if [ "$BIND_PATH" -ne 1 ]; then
  if [ -d "$HOME/$SUBDIRECTORY" ]; then
    echo "Subdirectory '$HOME/$SUBDIRECTORY' already exists."
    
    echo "Mounted."
  fi
  BINDPOINT=$(echo $HOME)
else
  mount --bind $MOUNTPOINT $BINDPOINT
  echo "'$MOUNTPOINT' mounted to '$BINDPOINT'."
fi
