#!/bin/bash

# Enable elxreno's copr repository and install preload for system performance improvements
echo "Enabling elxreno's COPR repository and installing preload..."
sudo dnf copr enable -y elxreno/preload && sudo dnf install -y preload

# Enable dawid's better_fonts COPR repository
echo "Enabling dawid's COPR repository for better fonts..."
sudo dnf copr enable -y chriscowleyunix/better_fonts

# Install fontconfig replacements and enhanced defaults for better font rendering
echo "Installing fontconfig font replacements..."
sudo dnf install -y fontconfig-font-replacements

echo "Installing fontconfig enhanced defaults..."
sudo dnf install -y fontconfig-enhanced-defaults

echo "Installing additional fonts..."
sudo dnf install material-icons-fonts fontawesome-fonts-all


echo "All tasks completed successfully."

