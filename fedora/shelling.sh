#!/bin/bash

# Check for curl or wget, install curl if neither is available
if command -v curl &> /dev/null; then
    downloader="curl -fsSL"
elif command -v wget &> /dev/null; then
    downloader="wget -qO-"
else
    echo "Neither curl nor wget is available. Installing curl..."
    sudo dnf install -y curl || sudo apt-get install -y curl || sudo yum install -y curl
    downloader="curl -fsSL"
fi

# Present shell options in a menu
echo "Select a shell to install:"
options=("Zsh" "Fish" "None")
select opt in "${options[@]}"; do
    case $opt in
        "Zsh")
            echo "Installing Zsh and Oh My Zsh..."
            sudo dnf install -y zsh || sudo apt-get install -y zsh || sudo yum install -y zsh
            sh -c "$($downloader https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
            chsh -s $(which zsh)
            break
            ;;
        "Fish")
            echo "Installing Fish..."
            sudo dnf install -y fish || sudo apt-get install -y fish || sudo yum install -y fish
            chsh -s $(which fish)
            break
            ;;
        "None")
            echo "No alternative shell selected."
            break
            ;;
        *)
            echo "Invalid option. Please choose again."
            ;;
    esac
done

# If no shell is chosen, prompt for Oh My Bash installation
if [[ "$opt" == "None" ]]; then
    read -p "Would you like to modify Bash and install Oh My Bash? (yes/Y) " choice
    case "$choice" in
        yes|Y)
            echo "Installing Oh My Bash..."
            bash -c "$($downloader https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh)"
            ;;
        *)
            echo "No changes made to Bash."
            ;;
    esac
fi

